﻿// <copyright file="FizzBuzzControllerTests.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// FizzBuzz Controller Tests
// </summary> 
namespace FizzBuzzTest.Controllers
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Mvc;
    using FizzBuzzApplication.Controllers;
    using FizzBuzzApplication.Models;
    using FizzBuzzServices.Interfaces;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Fizz Buzz Controller Tests
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// fizz buzz service mock instance
        /// </summary>
        private Mock<IFizzBuzzService> fizzBuzzServiceMock;

        /// <summary>
        /// fizz buzz controller instance
        /// </summary>
        private FizzBuzzController fizzBuzzController;

        /// <summary>
        /// fizz buzz model
        /// </summary>
        private FizzBuzzModel fizzBuzzModel;

        /// <summary>
        /// Test Fixture SetUp
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.fizzBuzzModel = new FizzBuzzModel();
            this.fizzBuzzServiceMock = new Mock<IFizzBuzzService>();
            this.fizzBuzzController = new FizzBuzzController(this.fizzBuzzServiceMock.Object);
        }

        /// <summary>
        /// Test Index Action View
        /// </summary>
        [TestCase]
        public void IndexAction_ShouldReturnIndexView()
        {
            // Act
            var resultView = this.fizzBuzzController.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Index", resultView.ViewName);
        }

        /// <summary>
        /// Test When Input is 2
        /// </summary>
        [TestCase]
        public void When_Input_is_2_GetList_Method_ShouldReturn_ListOfTwoElemets()
        {
            // Arrange
            this.fizzBuzzModel.Number = 2;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(2)).Returns(new List<string>() { "1", "2" });
            var expectedResultList = new List<string>() { "1", "2" };

            // Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            CollectionAssert.AreEqual(expectedResultList, actualResultModel.FizzBuzzList);
        }

        /// <summary>
        /// Test When Number Is Divisible By Three
        /// </summary>
        [TestCase]
        public void When_Number_Is_DivisibleByThree_ShouldReturn_Fizz_At_3_Index_Of_FizzBuzzList()
        {
            // Arrange
            this.fizzBuzzModel.Number = 3;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(3)).Returns(new List<string>() { "1", "2", "fizz" });
            var expectedResult = "fizz";

            // Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(2));
        }

        /// <summary>
        /// Test When Number Is Divisible By Five
        /// </summary>
        [TestCase]
        public void When_Number_Is_DivisibleByFive_ShouldReturn_Buzz_At_5_Index_Of_FizzBuzz_List()
        {
            // Arrange
            this.fizzBuzzModel.Number = 5;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(5)).Returns(new List<string>() { "1", "2", "fizz", "4", "buzz" });
            var expectedResult = "buzz";

            // Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(4));
        }

        /// <summary>
        /// Test When Number Is Divisible By Three And Five
        /// </summary>
        [TestCase]
        public void When_Number_Is_DivisibleByFiveAndThree_ShouldReturn_FizzAt3_BuzzAt5_FizzBuzzAt15_IndexOf_FizzBuzz_List()
        {
            // Arrange
            this.fizzBuzzModel.Number = 15;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(15)).Returns(new List<string>() 
            { 
                "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz"
            });
            var expectedResultAt3 = "fizz";
            var expectedResultAt5 = "buzz";
            var expectedResultAt15 = "fizz buzz";

            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.AreEqual(expectedResultAt3, actualResultModel.FizzBuzzList.ElementAt(2));
            Assert.AreEqual(expectedResultAt5, actualResultModel.FizzBuzzList.ElementAt(4));
            Assert.AreEqual(expectedResultAt15, actualResultModel.FizzBuzzList.ElementAt(14));
        }

        /// <summary>
        /// Test When Number Is Not Divisible By Three Or Five
        /// </summary>
        [TestCase]
        public void When_Number_Is_Not_DivisibleByFiveAndThree_ShouldReturn_SameNumber_At_Nth_Index_FizzBuzz_List()
        {
            // Arrange
            this.fizzBuzzModel.Number = 2;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(2)).Returns(new List<string>() { "1", "2" });
            var expectedResult = "2";

            // Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel);
            Assert.AreEqual(expectedResult, actualResultModel.FizzBuzzList.ElementAt(1));
        }

        /// <summary>
        /// Test When Number Is Greater Than 20
        /// </summary>
        [TestCase]
        public void When_Number_Is_Greater_Than20_ShouldReturn_20Items_In_FizzBuzz_List()
        {
            // Arrange
            this.fizzBuzzModel.Number = 22;
            this.fizzBuzzServiceMock.Setup(fbs => fbs.GetFizzBuzzList(22)).Returns(new List<string>() 
            { 
                "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz", "13", "14", "fizz buzz",
                "16", "17", "fizz", "19", "buzz", "fizz", "22"
            });

            // Act
            var actResult = this.fizzBuzzController.GetList(this.fizzBuzzModel) as ViewResult;
            var actualResultModel = actResult.Model as FizzBuzzModel;

            // Assert
            Assert.IsNotNull(actualResultModel.FizzBuzzList);
            Assert.AreEqual(20, actualResultModel.FizzBuzzList.Count());
        }
    }
}
