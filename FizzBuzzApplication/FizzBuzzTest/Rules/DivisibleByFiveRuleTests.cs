﻿// <copyright file="DivisibleByFiveRuleTests.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible By Five Rule Tests
// </summary> 
namespace FizzBuzzTest.Rules
{
    using System;
    using FizzBuzzRule;
    using FizzBuzzRule.Interfaces;
    using FizzBuzzRule.Rules;
    using NUnit.Framework;

    /// <summary>
    /// Divisible By Five Rule Tests
    /// </summary>
    [TestFixture]
    public class DivisibleByFiveRuleTests
    {
        /// <summary>
        /// divisible by Five Rule instance
        /// </summary>
        private IDivisibleRule divisiblebyFiveRule;

        /// <summary>
        /// Test Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.divisiblebyFiveRule = new DivisibleByFiveRule();
        }

        /// <summary>
        /// Test When Number DivisibleByFive
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_Buzz_AndRuleShouldPass()
        {
            // Act
            var result = this.divisiblebyFiveRule.Run(5, DayOfWeek.Monday);
            var ruleResult = this.divisiblebyFiveRule.Result;

            // Assert
            Assert.True(result);
            Assert.AreEqual(Constants.BUZZ, ruleResult);
        }

        /// <summary>
        /// Test When Number Not DivisibleByFive
        /// </summary>
        [TestCase]
        public void When_Number_Not_DivisibleByFive_ShouldReturn_EmptyString_AndRuleShouldFail()
        {
            // Act
            var result = this.divisiblebyFiveRule.Run(4, DayOfWeek.Monday);
            var ruleResult = this.divisiblebyFiveRule.Result;

            // Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }

        /// <summary>
        /// Test When Number Divisible By Five And It Wednesday
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByFive_And_Its_Wednesday_ShouldReturn_wuzz_AndRuleShouldPass()
        {
            // Act
            var result = this.divisiblebyFiveRule.Run(5, DayOfWeek.Wednesday);
            var ruleResult = this.divisiblebyFiveRule.Result;

            // Assert
            Assert.True(result);
            Assert.AreEqual(Constants.WUZZ, ruleResult);
        }
    }
}
