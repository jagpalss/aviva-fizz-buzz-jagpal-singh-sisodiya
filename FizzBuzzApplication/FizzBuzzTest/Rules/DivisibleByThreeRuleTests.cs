﻿// <copyright file="DivisibleByThreeRuleTests.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible By Three Rule Tests
// </summary> 
namespace FizzBuzzTest.Rules
{
    using System;
    using FizzBuzzRule;
    using FizzBuzzRule.Interfaces;
    using FizzBuzzRule.Rules;
    using NUnit.Framework;

    /// <summary>
    /// Divisible By Three Rule Tests
    /// </summary>
    [TestFixture]
    public class DivisibleByThreeRuleTests
    {
        /// <summary>
        /// divisible by Three Rule instance
        /// </summary>
        private IDivisibleRule divisiblebyThreeRule;

        /// <summary>
        ///  Test Setup
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.divisiblebyThreeRule = new DivisibleByThreeRule();
        }

        /// <summary>
        /// Test When Number Divisible By Three
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_Fizz_AndRuleShouldPass()
        {
            // Act
            var result = this.divisiblebyThreeRule.Run(3, DayOfWeek.Monday);
            var ruleResult = this.divisiblebyThreeRule.Result;

            // Assert
            Assert.True(result);
            Assert.AreEqual(Constants.FIZZ, ruleResult);
        }

        /// <summary>
        /// Test When Number Not Divisible By Three
        /// </summary>
        [TestCase]
        public void When_Number_Not_DivisibleByThree_ShouldReturn_EmptyString_AndRuleShouldFail()
        {
            // Act
            var result = this.divisiblebyThreeRule.Run(4, DayOfWeek.Monday);
            var ruleResult = this.divisiblebyThreeRule.Result;

            // Assert
            Assert.False(result);
            Assert.AreEqual(string.Empty, ruleResult);
        }

        /// <summary>
        /// Test When Number Divisible By Three And Its Wednesday
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThree_And_Its_WednesDay_ShouldReturn_Wizz_AndRuleShouldPass()
        {
            // Act
            var result = this.divisiblebyThreeRule.Run(3, DayOfWeek.Wednesday);
            var ruleResult = this.divisiblebyThreeRule.Result;

            // Assert
            Assert.True(result);
            Assert.AreEqual(Constants.WIZZ, ruleResult);
        }
    }
}
