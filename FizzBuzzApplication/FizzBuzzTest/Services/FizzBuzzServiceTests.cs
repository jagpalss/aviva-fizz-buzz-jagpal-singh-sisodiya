﻿// <copyright file="FizzBuzzServiceTests.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Fizz Buzz Service Tests
// </summary> 
namespace FizzBuzzTest.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzRule;
    using FizzBuzzRule.Interfaces;
    using FizzBuzzServices.Interfaces;
    using FizzBuzzServices.Services;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Fizz Buzz Service Tests
    /// </summary>
    [TestFixture]
    public class FizzBuzzServiceTests
    {
        /// <summary>
        /// divisible rule list
        /// </summary>
        private IList<IDivisibleRule> divisibleRuleList;

        /// <summary>
        /// divisible rule mocking instance
        /// </summary>
        private Mock<IDivisibleRule> divisibleRuleMock;

        /// <summary>
        /// divisible rule service mocking instance
        /// </summary>
        private Mock<IDivisibleRuleService> divisibleRuleServiceMock;

        /// <summary>
        /// fizz buzz service
        /// </summary>
        private IFizzBuzzService fizzbuzzService;

        /// <summary>
        /// Test Fixture SetUp
        /// </summary>
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleServiceMock = new Mock<IDivisibleRuleService>();
            this.fizzbuzzService = new FizzBuzzService(this.divisibleRuleServiceMock.Object);
        }

        /// <summary>
        /// test Setup
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();
        }

        /// <summary>
        /// Test When Number Divisible By Three
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_Fizz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.FIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            // Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        /// <summary>
        /// Test When Number Divisible By Five
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_Buzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.FIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.BUZZ);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { this.divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            // Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "fizz", "4", "buzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        /// <summary>
        /// Test When Number Divisible By Three And Five
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_ShouldReturn_Fizz_Buzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.FIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.BUZZ);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { this.divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            // Assert
            IList<string> expectedresult = new List<string>() 
            { 
                "1", "2", "fizz", "4", "buzz", "fizz", "7", "8", 
                "fizz", "buzz", "11", "fizz",
                "13", "14", "fizz buzz"
            };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        /// <summary>
        /// Test When Number Divisible By Three And Its Wednesday
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThree_WednesDay_ShouldReturn_Wizz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.WIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(3);

            // Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        /// <summary>
        /// Test When Number Divisible By Five And Its Wednesday
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByFive_WednesDay_ShouldReturn_Wuzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.WIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.WUZZ);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { this.divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(5);

            // Assert
            IList<string> expectedresult = new List<string>() { "1", "2", "wizz", "4", "wuzz" };
            CollectionAssert.AreEqual(expectedresult, result);
        }

        /// <summary>
        /// Test When Number Divisible By Three And Five Both And Its Wednesday
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_WednesDay_ShouldReturn_Wizz_Wuzz_InResultList()
        {
            // Arrange
            this.divisibleRuleMock.SetupGet(dr => dr.Result).Returns(Constants.WIZZ);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.SetupGet(i => i.Result).Returns(Constants.WUZZ);
            var divisibleRuleListForFive = new List<IDivisibleRule>() { this.divisibleRuleMock.Object };

            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 3 == 0))).Returns(this.divisibleRuleList);
            this.divisibleRuleServiceMock.Setup(drs => drs.GetMatchedRules(It.Is<int>(number => number % 5 == 0))).Returns(divisibleRuleListForFive);
            this.divisibleRuleServiceMock.Setup(d => d.GetMatchedRules(It.Is<int>(t => (t % 3 == 0 && t % 5 == 0)))).Returns(this.divisibleRuleList.Concat(divisibleRuleListForFive).AsEnumerable());

            // Act
            var result = this.fizzbuzzService.GetFizzBuzzList(15);

            // Assert
            IList<string> expectedresult = new List<string>()
            {
                "1", "2", "wizz", "4", "wuzz", "wizz", "7", "8", 
                "wizz", "wuzz", "11", "wizz",
                "13", "14", "wizz wuzz"
            };
            CollectionAssert.AreEqual(expectedresult, result);
        }
    }
}
