﻿// <copyright file="DivisibleRuleServiceTests.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible Rule Service Tests
// </summary> 
namespace FizzBuzzTest.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzRule.Interfaces;
    using FizzBuzzServices.Interfaces;
    using FizzBuzzServices.Services;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Divisible Rule Service Tests
    /// </summary>
    [TestFixture]
    public class DivisibleRuleServiceTests
    {
        /// <summary>
        /// divisible rule list
        /// </summary>
        private IList<IDivisibleRule> divisibleRuleList;

        /// <summary>
        /// divisible rule mocking instance
        /// </summary>
        private Mock<IDivisibleRule> divisibleRuleMock;

        /// <summary>
        /// divisible rule service
        /// </summary>
        private IDivisibleRuleService divisibleRuleService;

        /// <summary>
        /// Test Fixture Setup
        /// </summary>
        [TestFixtureSetUp]
        public void SetUp()
        {
            this.divisibleRuleList = new List<IDivisibleRule>();

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 3 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleMock = new Mock<IDivisibleRule>();
            this.divisibleRuleMock.Setup(dr => dr.Run(It.Is<int>(number => number % 5 == 0), It.IsAny<DayOfWeek>())).Returns(true);
            this.divisibleRuleList.Add(this.divisibleRuleMock.Object);

            this.divisibleRuleService = new DivisibleRuleService(this.divisibleRuleList);
        }

        /// <summary>
        /// Test When Number Divisible By Three
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThree_ShouldReturn_OneRule()
        {
            // Act
            var resultRules = this.divisibleRuleService.GetMatchedRules(3);

            // Assert
            Assert.AreEqual(1, resultRules.Count());
        }

        /// <summary>
        /// Test When Number Divisible By Five
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByFive_ShouldReturn_OneRule()
        {
            // Act
            var resultRules = this.divisibleRuleService.GetMatchedRules(5);

            // Assert
            Assert.AreEqual(1, resultRules.Count());
        }

        /// <summary>
        /// Test When Number Divisible By Three And Five
        /// </summary>
        [TestCase]
        public void When_Number_DivisibleByThreeAndFive_ShouldReturn_TwoRule()
        {
            // Act
            var resultRules = this.divisibleRuleService.GetMatchedRules(15);

            // Assert
            Assert.AreEqual(2, resultRules.Count());
        }

        /// <summary>
        /// Test When Number Is Not Divisible By Three And Five
        /// </summary>
        [TestCase]
        public void When_Number_Not_DivisibleByThreeOrFive_ShouldReturn_ZeroRule()
        {
            // Act
            var resultRules = this.divisibleRuleService.GetMatchedRules(7);

            // Assert
            Assert.AreEqual(0, resultRules.Count());
        }
    }
}
