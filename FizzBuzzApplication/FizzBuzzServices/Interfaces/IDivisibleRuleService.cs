﻿// <copyright file="IDivisibleRuleService.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible Rule Service interface
// </summary> 
namespace FizzBuzzServices.Interfaces
{
    using System.Collections.Generic;
    using FizzBuzzRule.Interfaces;

    /// <summary>
    /// Interface of DivisibleRuleService
    /// </summary>
    public interface IDivisibleRuleService
    {
        /// <summary>
        /// Function to get all rules which returned true for the given input
        /// </summary>
        /// <param name="number">input number</param>
        /// <returns>Enumerable of rules which returned true for the given input</returns>
        IEnumerable<IDivisibleRule> GetMatchedRules(int number);
    }
}
