﻿// <copyright file="IFizzBuzzService.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Fizz Buzz Service Interface
// </summary> 
namespace FizzBuzzServices.Interfaces
{
    using System.Collections.Generic;

    /// <summary>
    /// Interface for fizz buzz service
    /// </summary>
    public interface IFizzBuzzService
    {
        /// <summary>
        ///  Method to get fizz buzz list
        /// </summary>
        /// <param name="maxLimit">input number</param>
        /// <returns>Fizz buzz list</returns>
        IEnumerable<string> GetFizzBuzzList(int maxLimit);
    }
}
