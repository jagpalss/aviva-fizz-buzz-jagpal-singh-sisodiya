﻿// <copyright file="DivisibleRuleService.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible Rule Service
// </summary>
namespace FizzBuzzServices.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzRule.Interfaces;
    using FizzBuzzServices.Interfaces;

    /// <summary>
    /// Divisible Rule Service
    /// </summary>
    public class DivisibleRuleService : IDivisibleRuleService
    {
        /// <summary>
        /// List of rules which implements IDivisibleRule interface
        /// </summary>
        private readonly IEnumerable<IDivisibleRule> divisibleRuleList;

        /// <summary>
        /// Initializes a new instance of the <see cref="DivisibleRuleService"/> class
        /// </summary>
        /// <param name="divisibleRuleList">List of divisible rules</param>
        public DivisibleRuleService(IEnumerable<IDivisibleRule> divisibleRuleList)
        {
            this.divisibleRuleList = divisibleRuleList;
        }

        /// <summary>
        /// Function to get all rules which returned true for the given input
        /// </summary>
        /// <param name="number">input number</param>
        /// <returns>Enumerable of rules which returned true for the given input</returns>
        public IEnumerable<IDivisibleRule> GetMatchedRules(int number)
        {
            return this.divisibleRuleList.Where(rule => rule.Run(number, DateTime.UtcNow.DayOfWeek));
        }
    }
}
