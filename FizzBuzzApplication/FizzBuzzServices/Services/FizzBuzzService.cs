﻿// <copyright file="FizzBuzzService.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Fizz Buzz Service
// </summary> 
namespace FizzBuzzServices.Services
{
    using System.Collections.Generic;
    using System.Linq;
    using FizzBuzzServices.Interfaces;

    /// <summary>
    /// Fizz Buzz Service
    /// </summary>
    public class FizzBuzzService : IFizzBuzzService
    {
        /// <summary>
        /// DivisibleRule Service instance
        /// </summary>
        private readonly IDivisibleRuleService divisibleRuleServise;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzService"/> class
        /// </summary>
        /// <param name="divisibleRuleServiseObj">divisible Rule Service instance</param>
        public FizzBuzzService(IDivisibleRuleService divisibleRuleServiseObj)
        {
            this.divisibleRuleServise = divisibleRuleServiseObj;
        }

        /// <summary>
        ///  Method to get fizz buzz list
        /// </summary>
        /// <param name="maxLimit">input number</param>
        /// <returns>Fizz buzz list</returns>
        public IEnumerable<string> GetFizzBuzzList(int maxLimit)
        {
            var fizzBuzzList = new List<string>();

            for (var itrator = 1; itrator <= maxLimit; itrator++)
            {
                var ruleList = this.divisibleRuleServise.GetMatchedRules(itrator);
                if (ruleList.Any())
                {
                    var resultString = string.Join(" ", ruleList.Select(rule => rule.Result));
                    fizzBuzzList.Add(resultString.Trim());
                }
                else
                {
                    fizzBuzzList.Add(itrator.ToString());
                }
            }

            return fizzBuzzList;
        }
    }
}
