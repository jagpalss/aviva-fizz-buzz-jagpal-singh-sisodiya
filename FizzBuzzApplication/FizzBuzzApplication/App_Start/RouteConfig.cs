﻿// <copyright file="RouteConfig.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Route Config 
// </summary> 
namespace FizzBuzzApplication
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;

    /// <summary>
    /// Route Config
    /// </summary>
    public class RouteConfig
    {
        /// <summary>
        /// Register Application Routes
        /// </summary>
        /// <param name="routes">Route Collection</param>
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                 name: "Default",
                url: "{controller}/{action}",
                defaults: new { controller = "FizzBuzz", action = "Index" });
        }
    }
}