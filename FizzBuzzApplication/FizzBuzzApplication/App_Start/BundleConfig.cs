﻿// <copyright file="BundleConfig.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Bundle Config
// </summary> 
namespace FizzBuzzApplication.App_Start
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Optimization;

    /// <summary>
    /// Bundle all styles and scripts
    /// </summary>
    public static class BundleConfig
    {
        /// <summary>
        /// Register All Bundles
        /// </summary>
        /// <param name="bundles">Bundle Collection</param>
        public static void RegisterBundle(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/*.css"));
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                    "~/Scripts/jquery-1.8.2.*"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                      "~/Scripts/jquery.unobtrusive*",
                      "~/Scripts/jquery.validate*"));
        }
    }
}