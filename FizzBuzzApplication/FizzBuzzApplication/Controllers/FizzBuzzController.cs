﻿// <copyright file="FizzBuzzController.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// FizzBuzz Controller 
// </summary> 
namespace FizzBuzzApplication.Controllers
{
    using System;
    using System.Linq;
    using System.Web.Mvc;
    using FizzBuzzApplication.Models;
    using FizzBuzzServices.Interfaces;
    using PagedList;

    /// <summary>
    /// Fizz Buzz Controller
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Page Size
        /// </summary>
        private const int PageSize = 20;

        /// <summary>
        /// IFizzBuzzService Instance
        /// </summary>
        private readonly IFizzBuzzService fizzBuzzService;

        /// <summary>
        /// Initializes a new instance of the <see cref="FizzBuzzController"/> class
        /// </summary>
        /// <param name="fizzBuzzService">fizzBuzzService Instance</param>
        public FizzBuzzController(IFizzBuzzService fizzBuzzService)
        {
            this.fizzBuzzService = fizzBuzzService;
        }

        /// <summary>
        /// Index action
        /// </summary>
        /// <returns>Index view</returns>
        [HttpGet]
        public ActionResult Index()
        {
            ModelState.Clear();
            return this.View("Index");
        }

        /// <summary>
        /// action to get fizz buzz list
        /// </summary>
        /// <param name="fizzBuzzModel">fizz Buzz Model</param>
        /// <returns>Index view with updated model</returns>
        [HttpGet]
        public ActionResult GetList(FizzBuzzModel fizzBuzzModel)
        {
            if (ModelState.IsValid)
            {
                var fizzbuzzList = this.fizzBuzzService.GetFizzBuzzList(fizzBuzzModel.Number ?? 0);
                if ((fizzBuzzModel.Page ?? 1) * PageSize > fizzbuzzList.Count())
                {
                    fizzBuzzModel.Page = this.GetMaxPageNumber(fizzbuzzList.Count());
                }

                fizzBuzzModel.FizzBuzzList = fizzbuzzList.ToPagedList(fizzBuzzModel.Page ?? 1, PageSize);
            }

            return this.View("Index", fizzBuzzModel);
        }

        /// <summary>
        /// Method to get max page number of paged list
        /// </summary>
        /// <param name="totalItems">total items in list</param>
        /// <returns>max page number</returns>
        private int GetMaxPageNumber(double totalItems)
        {
            var mxPageSize = totalItems / PageSize;
            return (int)Math.Ceiling(mxPageSize);
        }
    }
}
