﻿// <copyright file="Global.asax.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Global asax file
// </summary> 
namespace FizzBuzzApplication
{
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using FizzBuzzApplication.App_Start;

    /// <summary>
    /// MVC Application
    /// </summary>
    public class MvcApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// Application Start
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            BundleConfig.RegisterBundle(BundleTable.Bundles);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}