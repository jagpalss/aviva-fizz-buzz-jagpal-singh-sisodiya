﻿// <copyright file="FizzBuzzModel.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// FizzBuzz Model 
// </summary> 
namespace FizzBuzzApplication.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using PagedList;

    /// <summary>
    /// Fizz Buzz Model
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Gets or sets the number
        /// </summary>
        [Required(ErrorMessage = "Input Required")]
        [Display(Name = "Please enter number")]
        [Range(1, 1000, ErrorMessage = "The input value must be between 1 and 1000")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter a numeric value")]
        public int? Number { get; set; }

        /// <summary>
        /// Gets or sets the fizz buzz list
        /// </summary>
        public IPagedList<string> FizzBuzzList { get; set; }

        /// <summary>
        /// Gets or sets the page number
        /// </summary>
        public int? Page { get; set; }
    }
}