﻿// <copyright file="DivisibleByFiveRule.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Divisible by 5 Rule
// </summary>
namespace FizzBuzzRule.Rules
{
    using System;
    using FizzBuzzRule.Interfaces;

    /// <summary>
    /// Rule to check divisibility of 5
    /// </summary>
    public class DivisibleByFiveRule : IDivisibleRule
    {
        /// <summary>
        /// Rule result
        /// </summary>
        private string result = string.Empty;

        /// <summary>
        /// Gets the result of rule
        /// </summary>
        public string Result
        {
            get { return this.result; }
        }

        /// <summary>
        /// To check rule divisibility conditions by 5 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        public bool Run(int input, DayOfWeek dayOfTheWeek)
        {
            if (input % 5 == 0)
            {
                this.result = dayOfTheWeek == DayOfWeek.Wednesday ? Constants.WUZZ : Constants.BUZZ;
                return true;
            }

            return false;
        }
    }
}
