﻿// <copyright file="Constants.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// Application Constants
// </summary> 
namespace FizzBuzzRule
{
    /// <summary>
    /// This contains all constants used in application
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// constant for FIZZ string
        /// </summary>
        public const string FIZZ = "fizz";

        /// <summary>
        /// constant for BUZZ string
        /// </summary>
        public const string BUZZ = "buzz";

        /// <summary>
        /// constant for WIZZ string
        /// </summary>
        public const string WIZZ = "wizz";

        /// <summary>
        /// constant for WUZZ string
        /// </summary>
        public const string WUZZ = "wuzz";
    }
}
