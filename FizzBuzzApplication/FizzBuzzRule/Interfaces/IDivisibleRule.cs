﻿// <copyright file="IDivisibleRule.cs" company="TCS"> 
// 2017</copyright> 
// <summary> 
// DivisibleRule interface
// </summary> 
namespace FizzBuzzRule.Interfaces
{
    using System;

    /// <summary>
    /// Interface for divisible rules
    /// </summary>
    public interface IDivisibleRule
    {
        /// <summary>
        /// Gets result of rule
        /// </summary>
        string Result { get; }

        /// <summary>
        /// To check rule divisibility conditions 
        /// </summary>
        /// <param name="input">input number to check divisibility</param>
        /// <param name="dayOfTheWeek">day Of The Week</param>
        /// <returns>true if rule pass else return false</returns>
        bool Run(int input, DayOfWeek dayOfTheWeek);
    }
}
